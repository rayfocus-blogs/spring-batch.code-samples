drop table if exists spring_batch.multithreaded_step_data;
create table spring_batch.multithreaded_step_data(
                                                     invoicenumber varchar(50),
                                                     stockcode varchar(50),
                                                     description varchar(100),
                                                     quantity varchar(5),
                                                     invoicedate varchar(20),
                                                     unitprice varchar(10),
                                                     customerid varchar(10),
                                                     country varchar(50)
);
