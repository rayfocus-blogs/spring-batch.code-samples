package com.rayfocus.codesamples.spring.batch.processor;

import com.rayfocus.codesamples.spring.domain.transaction.TransactionData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

public class TransactionDataProcessor implements ItemProcessor<TransactionData, TransactionData> {
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    @Override
    public TransactionData process(TransactionData item) throws Exception {
       // log.info("Processing {}",item);
        if(item.getInvoiceNumber() == null){
            return null;
        }
        return item;
    }
}
