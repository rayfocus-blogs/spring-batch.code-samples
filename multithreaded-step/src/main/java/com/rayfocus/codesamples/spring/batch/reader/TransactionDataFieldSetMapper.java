package com.rayfocus.codesamples.spring.batch.reader;

import com.rayfocus.codesamples.spring.domain.transaction.TransactionData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

public class TransactionDataFieldSetMapper implements FieldSetMapper<TransactionData> {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public TransactionData mapFieldSet(FieldSet fieldSet) throws BindException {
        TransactionData transactionData = new TransactionData();
        transactionData.setInvoiceNumber(fieldSet.readString("InvoiceNo"));
        transactionData.setStockCode(fieldSet.readString("StockCode"));
        transactionData.setDescription(fieldSet.readString("Description"));
        transactionData.setQuantity(fieldSet.readString("Quantity"));
        transactionData.setInvoiceDate(fieldSet.readString("InvoiceDate"));
        transactionData.setUnitPrice(fieldSet.readString("UnitPrice"));
        transactionData.setCustomerId(fieldSet.readString("CustomerID"));
        transactionData.setCountry(fieldSet.readString("Country"));
        return transactionData;
    }
}
