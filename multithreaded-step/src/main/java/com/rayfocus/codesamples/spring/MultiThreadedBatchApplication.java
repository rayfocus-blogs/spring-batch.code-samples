package com.rayfocus.codesamples.spring;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@Configuration
@ComponentScan(basePackages = { "com.rayfocus" })
@PropertySource("classpath:application.properties")
public class MultiThreadedBatchApplication {

	public static void main(String[] args) throws JobInstanceAlreadyCompleteException, JobExecutionAlreadyRunningException, JobParametersInvalidException, JobRestartException {
		ConfigurableApplicationContext applicationContext = SpringApplication.run(MultiThreadedBatchApplication.class, args);
		JobLauncher multiThreadedJobLauncher = applicationContext.getBean("multiThreadedJobLauncher",JobLauncher.class);
		Job multiThreadedJob = applicationContext.getBean("multiThreadedJob",Job.class);
		JobParameters multiThreadedJobParameters = new JobParametersBuilder()
				.addLong("time", System.currentTimeMillis())
				.toJobParameters();
		multiThreadedJobLauncher.run(multiThreadedJob,multiThreadedJobParameters);
		SpringApplication.exit(applicationContext);
	}
}
