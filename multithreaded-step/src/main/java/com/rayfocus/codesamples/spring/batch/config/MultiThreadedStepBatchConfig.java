package com.rayfocus.codesamples.spring.batch.config;


import com.amazonaws.services.s3.AmazonS3;
import com.rayfocus.codesamples.spring.batch.listener.SimpleJobExecutionListener;
import com.rayfocus.codesamples.spring.batch.reader.TransactionDataFieldSetMapper;
import com.rayfocus.codesamples.spring.batch.listener.SimpleStepExecutionListener;
import com.rayfocus.codesamples.spring.batch.processor.TransactionDataProcessor;
import com.rayfocus.codesamples.spring.batch.reader.TransactionDataSkipListener;
import com.rayfocus.codesamples.spring.domain.transaction.TransactionData;
import com.rayfocus.codesamples.spring.util.BatchUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.JobRegistry;
import org.springframework.batch.core.configuration.annotation.BatchConfigurer;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.configuration.support.JobRegistryBeanPostProcessor;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.job.builder.FlowBuilder;
import org.springframework.batch.core.job.flow.Flow;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.launch.support.SimpleJobOperator;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.step.tasklet.TaskletStep;
import org.springframework.batch.integration.config.annotation.EnableBatchIntegration;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.ItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileParseException;
import org.springframework.batch.item.file.MultiResourceItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.separator.DefaultRecordSeparatorPolicy;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.support.SynchronizedItemStreamReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.task.TaskExecutor;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;

@Configuration
@EnableIntegration
@EnableBatchIntegration
public class MultiThreadedStepBatchConfig {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private JobRepository jobRepository;

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private JobRegistry jobRegistry;

    @Autowired
    private JobExplorer jobExplorer;

    @Autowired
    @Qualifier("batchDataSource")
    private DataSource batchDataSource;

    @Autowired
    private PlatformTransactionManager transactionManager;

    @Value("${batch.chunk.size}")
    private int chunkSize;

    @Value("${batch.threadPool.size}")
    private int threadPoolSize;

    @Autowired
    private AmazonS3 amazonS3Client;

    @Autowired
    private ResourceLoader resourceLoader;

    @Bean
    public BatchConfigurer configurer(EntityManagerFactory entityManagerFactory) {
        return new CommonBatchConfig(entityManagerFactory);
    }

    @Bean
    public JobRegistryBeanPostProcessor jobRegistryBeanPostProcessor(JobRegistry jobRegistry) {
        JobRegistryBeanPostProcessor jobRegistryBeanPostProcessor = new JobRegistryBeanPostProcessor();
        jobRegistryBeanPostProcessor.setJobRegistry(jobRegistry);
        return jobRegistryBeanPostProcessor;
    }

    public MultiThreadedStepBatchConfig(JobBuilderFactory jobBuilderFactory,
                                      StepBuilderFactory stepBuilderFactory) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
    }

    @Bean
    public JobLauncher multiThreadedJobLauncher() throws Exception {
        SimpleJobLauncher simpleJobLauncher = new SimpleJobLauncher();
        simpleJobLauncher.setJobRepository(jobRepository);
        simpleJobLauncher.afterPropertiesSet();
        return simpleJobLauncher;
    }

    @Bean
    public SimpleJobOperator jobOperator() throws Exception {
        SimpleJobOperator jobOperator = new SimpleJobOperator();
        jobOperator.setJobExplorer(jobExplorer);
        jobOperator.setJobRepository(jobRepository);
        jobOperator.setJobRegistry(jobRegistry);
        jobOperator.setJobLauncher(multiThreadedJobLauncher());
        return jobOperator;
    }

    @Bean
    public Job multiThreadedJob() throws Exception {
        FlowBuilder<Flow> dataProcessFlowBuilder = new FlowBuilder<Flow>("multiThreaded-flow");
        Flow dataProcessFlow = dataProcessFlowBuilder
                .start(dataProcessStep())
                    .on(ExitStatus.COMPLETED.getExitCode()).end()
                .from(dataProcessStep())
                    .on(BatchUtil.ANY_OTHER_EXIT_STATUS).fail()
                .end();
        return jobBuilderFactory.get("multiThreaded-dataProcess-job")
                .incrementer(new RunIdIncrementer())
                .start(dataProcessFlow)
                .end()
                .listener(jobExecutionListener())
                .build();
    }

    // DATA-PROCESS STEP
    @Bean
    public Step dataProcessStep() throws Exception {
        Step step = this.stepBuilderFactory.get("dataProcess-step")
                .<TransactionData, TransactionData>chunk(chunkSize)
                .reader(dataReader())
                .faultTolerant()
                    .skip(FlatFileParseException.class)
                    .skipLimit(3)
                    .listener(transactionDataSkipListener())
                .processor(dataProcessor(null))
                .writer(dataWriter(null))
                .taskExecutor(taskExecutor())
                .build();
        ((TaskletStep) step).setStepExecutionListeners(new StepExecutionListener[]{
                stepExecutionListener()
        });
        return step;
    }

    // Skip listener
    @Bean
    @StepScope
    public TransactionDataSkipListener transactionDataSkipListener(){
        return new TransactionDataSkipListener();
    }

    /** Transaction data process Configuration - Start */

    // READER
    @Bean(destroyMethod="")
    @StepScope
    public SynchronizedItemStreamReader<TransactionData> dataReader() {
        SynchronizedItemStreamReader synchronizedItemStreamReader = new SynchronizedItemStreamReader();
        List<Resource> resourceList = new ArrayList<>();
        resourceList.add(new ClassPathResource("data_1.csv"));
        Resource[] resources = resourceList.toArray(new Resource[resourceList.size()]);
        MultiResourceItemReader<TransactionData> multiResourceItemReader = new MultiResourceItemReader<>();
        multiResourceItemReader.setName("multiResource-dataReader");
        multiResourceItemReader.setResources(resources);
        multiResourceItemReader.setDelegate(fileReader());
        synchronizedItemStreamReader.setDelegate(multiResourceItemReader);
        return synchronizedItemStreamReader;
    }

    @Bean
    @StepScope
    public FlatFileItemReader<TransactionData> fileReader()
    {
        FlatFileItemReader<TransactionData> reader = new FlatFileItemReader<>();
        reader.setLinesToSkip(1);
        reader.setSaveState(false);
        DefaultLineMapper<TransactionData> transactionDataLineMapper = new DefaultLineMapper();
        DelimitedLineTokenizer tokenizer = new DelimitedLineTokenizer();
        tokenizer.setNames(new String[] {
                "InvoiceNo","StockCode","Description","Quantity","InvoiceDate","UnitPrice","CustomerID","Country"
        });
        transactionDataLineMapper.setFieldSetMapper(transactionDataFieldSetMapper());
        transactionDataLineMapper.setLineTokenizer(tokenizer);
        reader.setLineMapper(transactionDataLineMapper);
        reader.setRecordSeparatorPolicy(new DefaultRecordSeparatorPolicy());
        return reader;
    }

    @Bean
    public FieldSetMapper<TransactionData> transactionDataFieldSetMapper(){
        return new TransactionDataFieldSetMapper();
    }

    // PROCESSOR
    @Bean
    @StepScope
    public ItemProcessor<TransactionData, TransactionData> dataProcessor(@Value("#{stepExecution}") StepExecution stepExecution) {
        return new TransactionDataProcessor();
    }

    // TASK EXECUTOR

    @Bean
    public TaskExecutor taskExecutor()
    {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(threadPoolSize);
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        executor.setThreadNamePrefix("exec-");
        return executor;
    }

    // WRITER
    @Bean
    @StepScope
    public ItemWriter dataWriter(@Qualifier("batchDataSource") DataSource dataSource) {
        JdbcBatchItemWriter<TransactionData> dataWriter = new JdbcBatchItemWriter<>();
        dataWriter.setDataSource(dataSource);
        dataWriter.setJdbcTemplate(namedParamJdbcTemplate(dataSource));
        dataWriter.setSql("insert into spring_batch.multithreaded_step_data (invoicenumber, stockcode, description, quantity, invoicedate, unitprice, " +
                "                                     customerid, country) " +
                "values (:invoiceNumber, :stockCode, :description, :quantity, :invoiceDate, :unitPrice, :customerId, :country) ");
        ItemSqlParameterSourceProvider<TransactionData> paramProvider =
                    new BeanPropertyItemSqlParameterSourceProvider<>();
        dataWriter.setItemSqlParameterSourceProvider(paramProvider);
            return dataWriter;
    }

    @Bean
    public NamedParameterJdbcTemplate namedParamJdbcTemplate(DataSource dataSource) {
        return new NamedParameterJdbcTemplate(dataSource);
    }

    // Step Listener
    @Bean
    public SimpleStepExecutionListener stepExecutionListener(){
        SimpleStepExecutionListener stepExecutionListener = new SimpleStepExecutionListener();
        return stepExecutionListener;
    }

    // Job Listener
    @Bean
    public JobExecutionListener jobExecutionListener() {
        return new SimpleJobExecutionListener();
    }

    /** Transaction data process Configuration - End */
}
