package com.rayfocus.codesamples.spring.batch.processor;

import com.rayfocus.codesamples.spring.domain.movie.Movie;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

public class MovieDataProcessor implements ItemProcessor<Movie, Movie> {
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    @Override
    public Movie process(Movie item) throws Exception {
        log.info("Processing movie data {}",item);
        return item;
    }
}
