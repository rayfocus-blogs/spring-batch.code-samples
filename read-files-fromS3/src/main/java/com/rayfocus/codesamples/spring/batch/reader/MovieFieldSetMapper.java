package com.rayfocus.codesamples.spring.batch.reader;

import com.rayfocus.codesamples.spring.domain.movie.Movie;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

public class MovieFieldSetMapper implements FieldSetMapper<Movie> {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public Movie mapFieldSet(FieldSet fieldSet) throws BindException {
        Movie movie = new Movie();
        movie.setName(fieldSet.readString("name"));
        movie.setGenre(fieldSet.readString("genre"));
        movie.setReleaseYear(fieldSet.readString("releaseYear"));
        movie.setReleasePlatform(fieldSet.readString("releasePlatform"));
        return movie;
    }
}
