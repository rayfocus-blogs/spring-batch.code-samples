package com.rayfocus.codesamples.spring;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@Configuration
@ComponentScan(basePackages = { "com.rayfocus" })
@PropertySource("classpath:application.properties")
public class ReadFileFromS3BatchApplication {

	public static void main(String[] args) throws JobInstanceAlreadyCompleteException, JobExecutionAlreadyRunningException, JobParametersInvalidException, JobRestartException {
		ConfigurableApplicationContext applicationContext = SpringApplication.run(ReadFileFromS3BatchApplication.class, args);
		JobLauncher movieDataProcessJobLauncher = applicationContext.getBean("movieDataProcessjobLauncher",JobLauncher.class);
		Job movieDataFromS3Job = applicationContext.getBean("processMovieDataFromS3Job",Job.class);
		JobParameters movieDataProcessJobParameters = new JobParametersBuilder()
				.addLong("time", System.currentTimeMillis())
				.toJobParameters();
		movieDataProcessJobLauncher.run(movieDataFromS3Job,movieDataProcessJobParameters);
		SpringApplication.exit(applicationContext);
	}
}
