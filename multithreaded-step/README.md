# Multithreaded scaling option with Spring batch

__Prerequisites__

- MySQL database in local machine (If not you can use H2 database and change data source config accordingly)
- Spring batch metadata tables (Script file is added under the src/main/resources in the code sample)
- Database table to persist the data by ItemWriter (Script file is added under the src/main/resources in the code sample)
