package com.rayfocus.codesamples.spring.batch.reader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.SkipListener;

public class TransactionDataSkipListener implements SkipListener {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public void onSkipInRead(Throwable t) {
        log.info("Skip on read due to {}",t.getMessage());
    }

    @Override
    public void onSkipInWrite(Object item, Throwable t) {
        log.info("Skip on write due to {} for item {}",t.getMessage(), item.toString());
    }

    @Override
    public void onSkipInProcess(Object item, Throwable t) {
        log.info("Skip on process due to {} for item {}",t.getMessage(), item.toString());
    }
}
