package com.rayfocus.codesamples.spring.batch.config;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBatchProcessing(modular = false)
public class AppMainConfig {
    // App configs...
}
