package com.rayfocus.codesamples.spring.batch.config;


import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.rayfocus.codesamples.spring.batch.listener.SimpleJobExecutionListener;
import com.rayfocus.codesamples.spring.batch.listener.SimpleStepExecutionListener;
import com.rayfocus.codesamples.spring.batch.processor.MovieDataProcessor;
import com.rayfocus.codesamples.spring.batch.reader.MovieFieldSetMapper;
import com.rayfocus.codesamples.spring.batch.writer.NoOpItemWriter;
import com.rayfocus.codesamples.spring.batch.writer.SimpleAsyncItemWriter;
import com.rayfocus.codesamples.spring.domain.movie.Movie;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.JobRegistry;
import org.springframework.batch.core.configuration.annotation.BatchConfigurer;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.configuration.support.JobRegistryBeanPostProcessor;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.job.builder.FlowBuilder;
import org.springframework.batch.core.job.flow.Flow;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.launch.support.SimpleJobOperator;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.step.tasklet.TaskletStep;
import org.springframework.batch.integration.async.AsyncItemProcessor;
import org.springframework.batch.integration.config.annotation.EnableBatchIntegration;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.MultiResourceItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.separator.DefaultRecordSeparatorPolicy;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.support.SynchronizedItemStreamReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.task.TaskExecutor;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;

import static com.rayfocus.codesamples.spring.util.BatchUtil.*;

@Configuration
@EnableIntegration
@EnableBatchIntegration
public class ReadFilesFromS3BatchConfig {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private JobRepository jobRepository;

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private JobRegistry jobRegistry;

    @Autowired
    private JobExplorer jobExplorer;

    @Autowired
    private PlatformTransactionManager transactionManager;

    @Value("${batch.chunk.size}")
    private int chunkSize;

    @Value("${batch.threadPool.size}")
    private int threadPoolSize;

    @Value("${rawdata.s3bucket}")
    private String rawDataS3Bucket;

    @Value("${rawdata.s3object.prefix}")
    private String rawDataS3ObjectPrefix;

    @Value("${inputData.fileExtension}")
    private String inputDataFileExtension;

    @Autowired
    private AmazonS3 amazonS3Client;

    @Autowired
    private ResourceLoader resourceLoader;

    @Bean
    public BatchConfigurer configurer(EntityManagerFactory entityManagerFactory) {
        return new CommonBatchConfig(entityManagerFactory);
    }

    @Bean
    public JobRegistryBeanPostProcessor jobRegistryBeanPostProcessor(JobRegistry jobRegistry) {
        JobRegistryBeanPostProcessor jobRegistryBeanPostProcessor = new JobRegistryBeanPostProcessor();
        jobRegistryBeanPostProcessor.setJobRegistry(jobRegistry);
        return jobRegistryBeanPostProcessor;
    }

    public ReadFilesFromS3BatchConfig(JobBuilderFactory jobBuilderFactory,
                                      StepBuilderFactory stepBuilderFactory) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
    }

    @Bean
    public JobLauncher movieDataProcessjobLauncher() throws Exception {
        SimpleJobLauncher simpleJobLauncher = new SimpleJobLauncher();
        simpleJobLauncher.setJobRepository(jobRepository);
        simpleJobLauncher.afterPropertiesSet();
        return simpleJobLauncher;
    }

    @Bean
    public SimpleJobOperator jobOperator() throws Exception {
        SimpleJobOperator jobOperator = new SimpleJobOperator();
        jobOperator.setJobExplorer(jobExplorer);
        jobOperator.setJobRepository(jobRepository);
        jobOperator.setJobRegistry(jobRegistry);
        jobOperator.setJobLauncher(movieDataProcessjobLauncher());
        return jobOperator;
    }

    @Bean
    public Job processMovieDataFromS3Job() throws Exception {
        FlowBuilder<Flow> dataProcessFlowBuilder = new FlowBuilder<Flow>("dataProcess-flow");
        Flow dataProcessFlow = dataProcessFlowBuilder
                .start(movieDataProcessStep())
                    .on(ExitStatus.COMPLETED.getExitCode()).end()
                .from(movieDataProcessStep())
                    .on(ANY_OTHER_EXIT_STATUS).fail()
                .end();
        return jobBuilderFactory.get("movieDataProcess-job")
                .incrementer(new RunIdIncrementer())
                .start(dataProcessFlow)
                .end()
                .listener(movieJobExecutionListener())
                .build();
    }

    // DATA-PROCESS STEP
    @Bean
    public Step movieDataProcessStep() throws Exception {
        Step step = this.stepBuilderFactory.get("dataProcess-step")
                .<Movie, Movie>chunk(chunkSize)
                .reader(movieDataReader())
                .processor(asyncMovieDataProcessor())
                .writer(asyncMovieItemWriter())
                .build();
        ((TaskletStep) step).setStepExecutionListeners(new StepExecutionListener[]{
                movieStepExecutionListener()
        });
        return step;
    }
    /** Movie data process Configuration - Start */

    // READER
    @Bean(destroyMethod="")
    @StepScope
    public SynchronizedItemStreamReader<Movie> movieDataReader() {
        SynchronizedItemStreamReader synchronizedItemStreamReader = new SynchronizedItemStreamReader();
        List<Resource> resourceList = new ArrayList<>();
        String sourceBucket = rawDataS3Bucket;
        String sourceObjectPrefix = rawDataS3ObjectPrefix
                .concat("MOVIES")
                .concat(FORWARD_SLASH);
        log.info("sourceObjectPrefix::"+sourceObjectPrefix);
        ListObjectsRequest listObjectsRequest = new ListObjectsRequest()
                .withBucketName(sourceBucket)
                .withPrefix(sourceObjectPrefix);
        ObjectListing sourceObjectsListing;
        do{
            sourceObjectsListing = amazonS3Client.listObjects(listObjectsRequest);
            for (S3ObjectSummary sourceFile : sourceObjectsListing.getObjectSummaries()){

                if(!(sourceFile.getSize() > 0)
                        || (!sourceFile.getKey().endsWith(DOT.concat(inputDataFileExtension)))
                ){
                    // Skip if file is empty (or) file extension is not "csv"
                    continue;
                }
                log.info("Reading "+sourceFile.getKey());
                resourceList.add(resourceLoader.getResource(S3_PROTOCOL_PREFIX.concat(sourceBucket).concat(FORWARD_SLASH)
                        .concat(sourceFile.getKey())));
            }
            listObjectsRequest.setMarker(sourceObjectsListing.getNextMarker());
        }while(sourceObjectsListing.isTruncated());

        Resource[] resources = resourceList.toArray(new Resource[resourceList.size()]);
        MultiResourceItemReader<Movie> multiResourceItemReader = new MultiResourceItemReader<>();
        multiResourceItemReader.setName("movie-multiResource-Reader");
        multiResourceItemReader.setResources(resources);
        multiResourceItemReader.setDelegate(movieFileItemReader());
        synchronizedItemStreamReader.setDelegate(multiResourceItemReader);
        return synchronizedItemStreamReader;
    }

    @Bean
    @StepScope
    public FlatFileItemReader<Movie> movieFileItemReader()
    {
        FlatFileItemReader<Movie> reader = new FlatFileItemReader<>();
        reader.setLinesToSkip(1);
        DefaultLineMapper<Movie> movieDataLineMapper = new DefaultLineMapper();
        DelimitedLineTokenizer tokenizer = new DelimitedLineTokenizer();
        tokenizer.setNames(new String[] {
                "name","genre","releaseYear","releasePlatform"
        });
        movieDataLineMapper.setFieldSetMapper(movieFieldSetMapper());
        movieDataLineMapper.setLineTokenizer(tokenizer);
        reader.setLineMapper(movieDataLineMapper);
        reader.setRecordSeparatorPolicy(new DefaultRecordSeparatorPolicy());
        return reader;
    }

    @Bean
    public FieldSetMapper<Movie> movieFieldSetMapper(){
        return new MovieFieldSetMapper();
    }

    // PROCESSOR
    // ASYNC ITEM PROCESSOR
    @Bean
    @StepScope
    public ItemProcessor asyncMovieDataProcessor(){
        AsyncItemProcessor<Movie, Movie> asyncItemProcessor = new AsyncItemProcessor<>();
        asyncItemProcessor.setDelegate(movieDataProcessor(null));
        asyncItemProcessor.setTaskExecutor(asyncMovieTaskExecutor());
        return asyncItemProcessor;
    }

    // DELEGATE ITEM PROCESSOR
    @Bean
    @StepScope
    public ItemProcessor<Movie, Movie> movieDataProcessor(@Value("#{stepExecution}") StepExecution stepExecution) {
        MovieDataProcessor movieDataProcessor = new MovieDataProcessor();
        return movieDataProcessor;
    }

    // TASK EXECUTOR

    @Bean
    public TaskExecutor asyncMovieTaskExecutor()
    {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(threadPoolSize);
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        executor.setThreadNamePrefix("movieExec-");
        return executor;
    }

    // WRITER
    @Bean
    @StepScope
    public ItemWriter asyncMovieItemWriter() throws Exception{
        SimpleAsyncItemWriter<Movie> simpleAsyncItemWriter = new SimpleAsyncItemWriter<>();
        simpleAsyncItemWriter.setDelegate(movieNoOpItemWriter());
        return simpleAsyncItemWriter;
    }

    @Bean
    @StepScope
    public ItemWriter movieNoOpItemWriter() {
        return new NoOpItemWriter();
    }

    // Step Listener
    @Bean
    public SimpleStepExecutionListener movieStepExecutionListener(){
        SimpleStepExecutionListener stepExecutionListener = new SimpleStepExecutionListener();
        return stepExecutionListener;
    }

    // Job Listener
    @Bean
    public JobExecutionListener movieJobExecutionListener() {
        return new SimpleJobExecutionListener();
    }

    /** Movie data process Configuration - End */
}
